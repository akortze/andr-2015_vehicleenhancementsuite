package com.example.threemuskinterns.vehicleenhancementsystem;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Andrew on 3/4/2015.
 */
public class CustomListViewAdapter extends ArrayAdapter<RowItem> {

    Context context;


    public CustomListViewAdapter(Context context, int resourceId, List<RowItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView smsSender;
        Button callNumber;
    }


    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        RowItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_items, parent, false);
            holder = new ViewHolder();
            holder.smsSender = (TextView) convertView.findViewById(R.id.numberText);
            holder.callNumber = (Button) convertView.findViewById(R.id.callNumber);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.smsSender.setText(MainActivity.getInstance().textList[position]);


        holder.callNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + MainActivity.getInstance().numList[position]));
                context.startActivity(callIntent);
            }
        });

        return convertView;
    }




}