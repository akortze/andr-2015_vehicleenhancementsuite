package com.example.threemuskinterns.vehicleenhancementsystem.fragments;


import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.example.threemuskinterns.vehicleenhancementsystem.LocationActivity;
import com.example.threemuskinterns.vehicleenhancementsystem.MainActivity;
import com.example.threemuskinterns.vehicleenhancementsystem.R;
import com.example.threemuskinterns.vehicleenhancementsystem.VeloText;

import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    VeloText mapsText;
    VeloText mapsOffText;
    Switch mapsSwitch;
    Switch trafficSwitch;
    Switch speedCheckSwitch;
    Switch gpsSwitch;
    Switch smsSwitch;
    Switch vesFullSwitch;
    Button startButton;
    VeloText trafficText;
    VeloText rearviewText;
    VeloText gpsText;
    VeloText trafficOffText;
    VeloText rearviewOffText;
    VeloText gpsOffText;
    VeloText smsText;
    VeloText smsOffText;
    VeloText allText;
    VeloText allOffText;
    ProgressBar progressBar;
    public double geoLatitude;
    public double geoLongitude;

    public HomeFragment() {
        // Required empty public constructor
    }

    public void onStart() {
        super.onStart();
//        getLocationFromAddress("4712 Pepper Drive Rockford IL 61114");
//        MainActivity.getInstance().geocodeLatitude = geoLatitude;
//        MainActivity.getInstance().geocodeLongitude = geoLongitude;
//        getAddressFromLocation(32.9671540,-96.8132270);


        progressBar.setProgress(0);
        startButton.setClickable(true);

        if (MainActivity.getInstance().switchesActivated == 0) {
            startButton.setText("----");
            startButton.setTextColor(getResources().getColor(R.color.grey));
        } else {
            startButton.setText("Start");
            startButton.setTextColor(getResources().getColor(R.color.turq));
        }

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (MainActivity.getInstance().switchesActivated == 0) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please make a Selection", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Initializing Navigation...", Toast.LENGTH_LONG).show();
                    startButton.setClickable(false);
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            for (int i = 0; i <= 105; i++) {
                                final int progress = i;
                                try {
                                    Thread.sleep(10);
                                } catch (Exception e) {}
                                progressBar.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setProgress(progress);
                                    }
                                });
                            }
                            // execute stuff here
                            Intent intent = new Intent(getActivity(), LocationActivity.class);
                            startActivity(intent);
                        }
                    };
                    new Thread(runnable).start();
                }
            }
        });

        vesFullSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (vesFullSwitch.isChecked()) {
                    allText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().vesFullSwitch = true;
                    allOffText.setTextColor(getResources().getColor(R.color.turq));
                    allOffText.setText("ON");
                    mapsSwitch.setChecked(true);
                    trafficSwitch.setChecked(true);
                    speedCheckSwitch.setChecked(true);
                    gpsSwitch.setChecked(true);
                    smsSwitch.setChecked(true);
                    MainActivity.getInstance().switchesActivated = 5;


                } else {
                    allText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().vesFullSwitch = false;
                    allOffText.setText("OFF");
                    allOffText.setTextColor(getResources().getColor(R.color.white));
//                    mapsSwitch.setChecked(false);
//                    trafficSwitch.setChecked(false);
//                    speedCheckSwitch.setChecked(false);
//                    gpsSwitch.setChecked(false);
//                    smsSwitch.setChecked(false);
                    MainActivity.getInstance().switchesActivated--;
                }
                if (MainActivity.getInstance().switchesActivated == 0) {
                    startButton.setText("----");
                    startButton.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    startButton.setText("Start");
                    startButton.setTextColor(getResources().getColor(R.color.turq));
                }
            }
        });

        smsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (smsSwitch.isChecked()) {
                    smsText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().smsSwitch = true;
                    smsOffText.setTextColor(getResources().getColor(R.color.turq));
                    smsOffText.setText("ON");
                    MainActivity.getInstance().switchesActivated++;
                } else {
                    smsText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().smsSwitch = false;
                    smsOffText.setText("OFF");
                    smsOffText.setTextColor(getResources().getColor(R.color.white));
                    MainActivity.getInstance().switchesActivated--;
                    vesFullSwitch.setChecked(false);
                }
                if (MainActivity.getInstance().switchesActivated == 0) {
                    startButton.setText("----");
                    startButton.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    startButton.setText("Start");
                    startButton.setTextColor(getResources().getColor(R.color.turq));
                }
            }
        });

        mapsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mapsSwitch.isChecked()) {
                    mapsText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().mapsSwitch = true;
                    mapsOffText.setText("ON");
                    mapsOffText.setTextColor(getResources().getColor(R.color.turq));
                    trafficSwitch.setChecked(true);
                    trafficOffText.setText("ON");
                    trafficText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().switchesActivated++;
                } else {
                    mapsText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().mapsSwitch = false;
                    mapsOffText.setText("OFF");
                    mapsOffText.setTextColor(getResources().getColor(R.color.white));
                    trafficSwitch.setChecked(false);
                    trafficOffText.setText("OFF");
                    trafficText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().switchesActivated--;
                    vesFullSwitch.setChecked(false);
                }
                if (MainActivity.getInstance().switchesActivated == 0) {
                    startButton.setText("----");
                    startButton.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    startButton.setText("Start");
                    startButton.setTextColor(getResources().getColor(R.color.turq));
                }
            }
        });
        trafficSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (trafficSwitch.isChecked()) {
                    trafficText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().trafficSwitch = true;
                    trafficOffText.setTextColor(getResources().getColor(R.color.turq));
                    trafficOffText.setText("ON");
                } else {
                    trafficText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().trafficSwitch = false;
                    trafficOffText.setText("OFF");
                    trafficOffText.setTextColor(getResources().getColor(R.color.white));
                    vesFullSwitch.setChecked(false);
                }
                if (!mapsSwitch.isChecked()) {
                    trafficText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().trafficSwitch = false;
                    trafficOffText.setText("OFF");
                    trafficOffText.setTextColor(getResources().getColor(R.color.white));
                    trafficSwitch.setChecked(false);
                }
                if (MainActivity.getInstance().switchesActivated == 0) {
                    startButton.setText("----");
                    startButton.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    startButton.setText("Start");
                    startButton.setTextColor(getResources().getColor(R.color.turq));
                }
            }
        });
        speedCheckSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (speedCheckSwitch.isChecked()) {
                    rearviewText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().speedCheckSwitch = true;
                    rearviewOffText.setText("ON");
                    rearviewOffText.setTextColor(getResources().getColor(R.color.turq));
                    MainActivity.getInstance().switchesActivated++;
                } else {
                    rearviewText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().speedCheckSwitch = false;
                    rearviewOffText.setText("OFF");
                    rearviewOffText.setTextColor(getResources().getColor(R.color.white));
                    MainActivity.getInstance().switchesActivated--;
                    vesFullSwitch.setChecked(false);
                }
                if (MainActivity.getInstance().switchesActivated == 0) {
                    startButton.setText("----");
                    startButton.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    startButton.setText("Start");
                    startButton.setTextColor(getResources().getColor(R.color.turq));
                }
            }
        });
        gpsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (gpsSwitch.isChecked()) {
                    gpsText.setTextColor(getResources().getColor(R.color.drawerColor));
                    MainActivity.getInstance().gpsSwitch = true;
                    gpsOffText.setText("ON");
                    gpsOffText.setTextColor(getResources().getColor(R.color.turq));
                    MainActivity.getInstance().switchesActivated++;
                } else {
                    gpsText.setTextColor(getResources().getColor(R.color.grey));
                    MainActivity.getInstance().gpsSwitch = false;
                    gpsOffText.setText("OFF");
                    gpsOffText.setTextColor(getResources().getColor(R.color.white));
                    MainActivity.getInstance().switchesActivated--;
                    vesFullSwitch.setChecked(false);
                }
                if (MainActivity.getInstance().switchesActivated == 0) {
                    startButton.setText("----");
                    startButton.setTextColor(getResources().getColor(R.color.grey));
                } else {
                    startButton.setText("Start");
                    startButton.setTextColor(getResources().getColor(R.color.turq));
                }
            }
        });
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View homeView = inflater.inflate(R.layout.fragment_home, container, false);

        mapsText = (VeloText) homeView.findViewById(R.id.mapsText);
        mapsSwitch = (Switch) homeView.findViewById(R.id.mapsSwitch);
        mapsOffText = (VeloText) homeView.findViewById(R.id.mapsOffText);
        trafficSwitch = (Switch) homeView.findViewById(R.id.trafficSwitch);
        speedCheckSwitch = (Switch) homeView.findViewById(R.id.speedCheckSwitch);
        gpsSwitch = (Switch) homeView.findViewById(R.id.gpsSwitch);
        trafficText = (VeloText) homeView.findViewById(R.id.trafficText);
        rearviewText = (VeloText) homeView.findViewById(R.id.speedCheckText);
        gpsText = (VeloText) homeView.findViewById(R.id.gpsText);
        trafficOffText = (VeloText) homeView.findViewById(R.id.trafficOffText);
        rearviewOffText = (VeloText) homeView.findViewById(R.id.rearViewOffText);
        gpsOffText = (VeloText) homeView.findViewById(R.id.gpsOffText);
        startButton = (Button) homeView.findViewById(R.id.startButton);
        progressBar = (ProgressBar) homeView.findViewById(R.id.progressBar);
        smsSwitch = (Switch) homeView.findViewById(R.id.smsSwitch);
        smsText = (VeloText) homeView.findViewById(R.id.smsText);
        smsOffText = (VeloText) homeView.findViewById(R.id.smsOffText);
        vesFullSwitch = (Switch) homeView.findViewById(R.id.allSwitch);
        allText = (VeloText) homeView.findViewById(R.id.allText);
        allOffText = (VeloText) homeView.findViewById(R.id.allOffText);

        if (MainActivity.getInstance().vesFullSwitch) {
            vesFullSwitch.setChecked(true);
            allText.setTextColor(getResources().getColor(R.color.drawerColor));
            allOffText.setText("ON");
            allOffText.setTextColor(getResources().getColor(R.color.turq));
        } else {
            vesFullSwitch.setChecked(false);
            allText.setTextColor(getResources().getColor(R.color.grey));
            smsOffText.setText("OFF");
            smsOffText.setTextColor(getResources().getColor(R.color.white));
        }

        if (MainActivity.getInstance().smsSwitch) {
            smsSwitch.setChecked(true);
            smsText.setTextColor(getResources().getColor(R.color.drawerColor));
            smsOffText.setText("ON");
            smsOffText.setTextColor(getResources().getColor(R.color.turq));
        } else {
            smsSwitch.setChecked(false);
            smsText.setTextColor(getResources().getColor(R.color.grey));
            smsOffText.setText("OFF");
            smsOffText.setTextColor(getResources().getColor(R.color.white));
        }
        if (MainActivity.getInstance().mapsSwitch) {
            mapsSwitch.setChecked(true);
            mapsText.setTextColor(getResources().getColor(R.color.drawerColor));
            mapsOffText.setText("ON");
            mapsOffText.setTextColor(getResources().getColor(R.color.turq));
        } else {
            mapsSwitch.setChecked(false);
            mapsText.setTextColor(getResources().getColor(R.color.grey));
            mapsOffText.setText("OFF");
            mapsOffText.setTextColor(getResources().getColor(R.color.white));
        }
        if (MainActivity.getInstance().trafficSwitch) {
            trafficSwitch.setChecked(true);
            trafficText.setTextColor(getResources().getColor(R.color.drawerColor));
            trafficOffText.setText("ON");
            trafficOffText.setTextColor(getResources().getColor(R.color.turq));
        } else {
            trafficSwitch.setChecked(false);
            trafficText.setTextColor(getResources().getColor(R.color.grey));
            trafficOffText.setText("OFF");
            trafficOffText.setTextColor(getResources().getColor(R.color.white));
        }
        if (MainActivity.getInstance().speedCheckSwitch) {
            speedCheckSwitch.setChecked(true);
            rearviewText.setTextColor(getResources().getColor(R.color.drawerColor));
            rearviewOffText.setText("ON");
            rearviewOffText.setTextColor(getResources().getColor(R.color.turq));
        } else {
            speedCheckSwitch.setChecked(false);
            rearviewText.setTextColor(getResources().getColor(R.color.grey));
            rearviewOffText.setText("OFF");
            rearviewOffText.setTextColor(getResources().getColor(R.color.white));
        }
        if (MainActivity.getInstance().gpsSwitch) {
            gpsSwitch.setChecked(true);
            gpsText.setTextColor(getResources().getColor(R.color.drawerColor));
            gpsOffText.setText("ON");
            gpsOffText.setTextColor(getResources().getColor(R.color.turq));
        } else {
            gpsSwitch.setChecked(false);
            gpsText.setTextColor(getResources().getColor(R.color.grey));
            gpsOffText.setText("OFF");
            gpsOffText.setTextColor(getResources().getColor(R.color.white));
        }
        return homeView;
    }


    public void getLocationFromAddress(String streetAddress) {

        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        // String newAddress = strAddress; // (String)saveas.getText().toString();
        try {
            List<Address> address = geocoder.getFromLocationName(streetAddress, 1);
            geoLatitude = address.get(0).getLatitude();
            geoLongitude = address.get(0).getLongitude();
        } catch (Exception e) {}
    }

    public String getAddressFromLocation(double geoLatitudeInput, double geoLongitudeInput) {

        Geocoder geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
        String locality = null;
        try {
            List<Address> address = geocoder.getFromLocation(geoLatitudeInput, geoLongitudeInput, 1);
            String result = String.format("%s, %s, %s", address.get(0).getMaxAddressLineIndex() > 0 ? address.get(0).getAddressLine(0) : "", address.get(0).getAdminArea(), address.get(0).getCountryName());
            String addressLine = address.get(0).getAddressLine(0);
            String adminArea = address.get(0).getAdminArea();
            String countryCode = address.get(0).getCountryCode();
            String countryName = address.get(0).getCountryName();
            String featureName = address.get(0).getFeatureName();
            locality = address.get(0).getLocality();
            String phone = address.get(0).getPhone();
            String postalCode = address.get(0).getPostalCode();
            String premises = address.get(0).getPremises();
            String subAdminArea = address.get(0).getSubAdminArea();
            String subLocality = address.get(0).getSubLocality();
            String subThoroughfare = address.get(0).getSubThoroughfare();
            String thoroughfare = address.get(0).getThoroughfare();
            String url = address.get(0).getUrl();
        } catch (Exception e) {}
        return locality;
    }

}
