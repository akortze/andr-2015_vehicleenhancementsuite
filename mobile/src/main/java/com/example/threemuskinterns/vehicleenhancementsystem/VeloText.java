package com.example.threemuskinterns.vehicleenhancementsystem;

/**
 * Created by Andrew on 7/17/2015.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Andrew on 7/17/2015.
 */
public class VeloText extends TextView {

    public VeloText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/Veloped.ttf"));
    }
}
