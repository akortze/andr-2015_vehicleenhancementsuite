package com.example.threemuskinterns.vehicleenhancementsystem;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import java.util.Date;

/**
 * Created by a0226042 on 7/16/2015.
 */
public class SMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        int i;
        String user;
        int userInt;
        //get the SMS message
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        String str = "";

        try {
            if (bundle != null) {
                //retrieve SMS message
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];
                for (i = 0; i < msgs.length; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    //str += "SMS from " + msgs[i].getOriginatingAddress();

                    switch (msgs[i].getDisplayOriginatingAddress()) {

                        default:
                            String incomingPhoneNumber = msgs[i].getDisplayOriginatingAddress();
                            if (!(incomingPhoneNumber.equals(MainActivity.getInstance().phoneNumber))) { // change to !(incomingPhoneNumber.equals...
                                notificationSetup(incomingPhoneNumber);
                                try {
                                    if (LocationActivity.getInstance().locationActivityActive && MainActivity.getInstance().smsSwitch) {
                                        if (!MainActivity.getInstance().getContactName(incomingPhoneNumber).equals("")) {
                                            MainActivity.getInstance().textList[MainActivity.getInstance().numOfTexts] = MainActivity.getInstance().getContactName(incomingPhoneNumber);
                                        } else {
                                            MainActivity.getInstance().textList[MainActivity.getInstance().numOfTexts] = incomingPhoneNumber;
                                        }
                                        MainActivity.getInstance().textContentsList[MainActivity.getInstance().numOfTexts] = msgs[i].getMessageBody();
                                        MainActivity.getInstance().numList[MainActivity.getInstance().numOfTexts] = incomingPhoneNumber;
                                        MainActivity.getInstance().newText = true;
                                        MainActivity.getInstance().numOfTexts++;
                                        SmsManager smsManager = SmsManager.getDefault();
                                        String sendText = "VES AutoText:\nThe person you have contacted is currently driving.";
                                        if (MainActivity.getInstance().mapsSwitch) {
                                            sendText = sendText + "\nLocation: " + "http://maps.google.com/maps?q=" + LocationActivity.getInstance().getLatitude + "," + LocationActivity.getInstance().getLongitude;
                                        }
                                        smsManager.sendTextMessage(incomingPhoneNumber, null, sendText, null, null);
                                        //smsManager.sendTextMessage("8152890244", null, "TEST", null, null);
                                    }
                                } catch (Exception e) {}
                            }
                        break;
                    }
                }
            }
        } catch (Exception e) {}
    }

    public void notificationSetup(String phoneNumber) {

        // build notification
        // the addAction re-use the same intent to keep the example short
        Notification notify  = new Notification.Builder(MainActivity.getInstance())
                .setContentTitle("Vehicle Enhancement Suite")
                .setContentText("Message received")
                .setSmallIcon(R.drawable.logoroundnt)
                .setStyle(new Notification.BigTextStyle().bigText("Message received"))
                .setAutoCancel(true)
                .setTicker("Message received from the following number: " + phoneNumber)
                .setWhen((new Date()).getTime()).build();

        NotificationManager notificationManager = (NotificationManager) MainActivity.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notify);
    }
}
