package com.example.threemuskinterns.vehicleenhancementsystem;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.os.Looper;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tomtom.lbs.sdk.TTMapView;
import com.tomtom.lbs.sdk.geolocation.ReverseGeocodeData;
import com.tomtom.lbs.sdk.geolocation.ReverseGeocodeListener;
import com.tomtom.lbs.sdk.geolocation.ReverseGeocodeOptionalParameters;
import com.tomtom.lbs.sdk.geolocation.ReverseGeocoder;
import com.tomtom.lbs.sdk.util.Coordinates;
import com.tomtom.lbs.sdk.util.SDKContext;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;

public class LocationActivity extends ActionBarActivity implements LocationListener {

    NeoText speedText;
    NeoText timeText;
    NeoText distanceText;
    NeoText altitudeText;
    NeoText accelerationText;
    NeoText speedLimit;
    NeoText locationText;
    NeoText roadText;
    NeoText trafficText;
    NeoText trafficSpeed;
    NeoText tempText;
    NeoText humidityText;
    NeoText addressText;

    Button stopButton;

    double distance = 0;
    double splitTime = 0;
    double previousSplit = 0;

    double accTime = 0;
    double previousAccTime = 0;
    double previousSpeed = 0;
    public double speedLimitVal = 0;
    public double trafficSpeedVal = 0;

    public long startTime = System.currentTimeMillis();
    public long currentTime = 0;
    public double elapsedTime = 0;
    public double travelTime = 0;
    public long reStartTime = System.currentTimeMillis();

    public boolean sentText = false;
    public boolean locationActivityActive = true;
    public boolean mapsEnabled = false;
    public boolean limitEnabled = false;
    public boolean noRearview = false;

    public double getLatitude;
    public double getLongitude;

    double currentSpeedMpS;
    double currentSpeedMph;
    double googleLatitude;
    double googleLongitude;

    public static LocationActivity ins;

    public static LocationActivity getInstance() {
        return ins;
    }

    ToneGenerator toneG_distance;
    ToneGenerator toneG_speed;

    beepThread_speed mBeepThread_speed;

    public MapView mMapView;
    public GoogleMap googleMap;
    public MarkerOptions marker;
    public CameraPosition cameraPosition;

    public TTMapView mapView;

    public int begin = 0;
    public byte[] bluetoothData = new byte[14];

    BluetoothDevice mDevice = null;
    ConnectThread mConnectThread=null;
    ConnectedThread mConnectedThread=null;

    ProgressBar verticalProgressBar1;
    ProgressBar verticalProgressBar2;
    //ProgressBar verticalProgressBar3;
    ProgressBar verticalProgressBar4;

    String bluetoothReceivedMessage = null;


    Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.getInstance().mapsSwitch && MainActivity.getInstance().speedCheckSwitch && MainActivity.getInstance().gpsSwitch) {
            setContentView(R.layout.location_layout_just_maps);
//            setContentView(R.layout.activity_location);
        } else if (MainActivity.getInstance().mapsSwitch && MainActivity.getInstance().speedCheckSwitch) {
            setContentView(R.layout.location_layout_just_maps);
//            setContentView(R.layout.activity_location);
        } else if (MainActivity.getInstance().mapsSwitch) {
            noRearview = true;
            setContentView(R.layout.location_layout_just_maps);
        } else if (MainActivity.getInstance().speedCheckSwitch) {
            setContentView(R.layout.location_layout_just_maps);
//            setContentView(R.layout.location_layout_just_rearview);
        } else {
            setContentView(R.layout.location_layout_just_maps);
//            setContentView(R.layout.activity_location);
        }

        // GENERAL
        ins = this;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        MainActivity.getInstance().locationActivityActive = true;

        // Turn on GPS
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!provider.contains("gps")) { //if gps is disabled
            this.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }

        // ACTIONBAR TEXT FONTS
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = LayoutInflater.from(this);
        View view = inflator.inflate(R.layout.titleview2_velo, null);
        ((VeloText) view.findViewById(R.id.title3)).setText("Navigation");
        this.getSupportActionBar().setCustomView(view);

        // SETUP MAPS
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        googleMap = mMapView.getMap();
        googleMap.setBuildingsEnabled(true);
        if (MainActivity.getInstance().trafficSwitch) {
            googleMap.setTrafficEnabled(true);
        } else {
            googleMap.setTrafficEnabled(false);
        }

        if (MainActivity.getInstance().mapsSwitch) {
            try {
                MapsInitializer.initialize(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            // create marker
            marker = new MarkerOptions().position(
                    new LatLng(0.0, 0.0)).title("Locating...");

            // Changing marker icon
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));

            // adding marker
            googleMap.addMarker(marker);
            cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(0.0, 0.0)).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
            // Perform any camera updates here
        }

        // SETUP SOUNDS
        toneG_distance = new ToneGenerator(AudioManager.STREAM_ALARM, 75);
        toneG_speed = new ToneGenerator(AudioManager.STREAM_ALARM, 75);


        // DECLARE VIEWS
        speedText = (NeoText) findViewById(R.id.initialSpeed);
        timeText = (NeoText) findViewById(R.id.initialTime);
        distanceText = (NeoText) findViewById(R.id.initialDistance);
        altitudeText = (NeoText) findViewById(R.id.initialAltitude);
        accelerationText = (NeoText) findViewById(R.id.initialAcceleration);
        speedLimit = (NeoText) findViewById(R.id.initial_limit);
//        locationText = (NeoText) findViewById(R.id.locationText);
//        roadText = (NeoText) findViewById(R.id.roadText);
        trafficText = (NeoText) findViewById(R.id.trafficText);
        trafficSpeed = (NeoText) findViewById(R.id.trafficSpeed);
        stopButton = (Button) findViewById(R.id.endButton);
        tempText = (NeoText) findViewById(R.id.initialTemp);
        humidityText = (NeoText) findViewById(R.id.initialHumidity);
        addressText = (NeoText) findViewById(R.id.address);
<<<<<<< HEAD
        verticalProgressBar1 = (ProgressBar) findViewById(R.id.progressBar);
        verticalProgressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
//        //verticalProgressBar3 = (ProgressBar) findViewById(R.id.progressBar3);
        verticalProgressBar4 = (ProgressBar) findViewById(R.id.progressBar4);
        //verticalProgressBar3.setVisibility(View.INVISIBLE);

        // INITIALIZE PROGRESS BARS
//        if (MainActivity.getInstance().rearviewSwitch) {
        if (!noRearview) {
            verticalProgressBar1.setSecondaryProgress(100);
            verticalProgressBar2.setSecondaryProgress(100);
            //verticalProgressBar3.setSecondaryProgress(100);
            verticalProgressBar4.setSecondaryProgress(100);
        }

=======
>>>>>>> 469f5da0cdf9cdd9e97b90a3b6b58a2b98885ef4

        //my api key
        SDKContext.setDeveloperKey("INSERT TOMTOM MAPKIT API KEY HERE");

        // BUTTON LISTENERS
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.getInstance().mTitle = "";
                MainActivity.getInstance().setActionBarTitle("Home");
                MainActivity.getInstance().setTitle("");
//                MainActivity.getInstance().onNavigationDrawerItemSelected(0);
//                NavigationDrawerFragment.getInstance().selectItem(0);
//                MainActivity.getInstance().wasLocationActivityClosed = true;
                locationActivityActive = false;
                MainActivity.getInstance().locationActivityActive = false;
                finish();
            }
        });

    }   // END OF ONCREATE


//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    // OTHER METHODS START HERE

    public void onStart() {
        super.onStart();
        //restoreVariables();
    }

    @Override
    protected void onPause() {
        //Kill all the threads
        super.onPause();

        if(mBeepThread_speed!=null) {
            mBeepThread_speed.cancel();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        //restoreVariables();
        //restoreVariables();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //saveVariables();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        MainActivity.getInstance().mTitle = "";
        MainActivity.getInstance().setActionBarTitle("Home");
        MainActivity.getInstance().setTitle("");
        locationActivityActive = false;
        MainActivity.getInstance().locationActivityActive = false;
        finish();
    }

    public class MyReverseGeocodeListener implements ReverseGeocodeListener {

        public void handleReverseGeocode(Vector<ReverseGeocodeData> data, Object payload) {

            if (data != null && data.size() > 0) {
                ReverseGeocodeData result = data.elementAt(0);
                speedLimitVal = result.maxSpeedKph * .621371;
                speedLimitVal = Math.round(speedLimitVal);

                if (result.maxSpeedKph > 2) {
                    speedLimit.setText("" + String.format("%.0f", speedLimitVal) + "");
                } else {
                    speedLimit.setText("N/A");
                }

//                if (result.street != null) {
///                   roadText.setText(result.street);
//                } else {
//                    roadText.setText("");
//                }
//
//                if ((getCityFromLocation(getLatitude, getLongitude) != null) && (result.state != null)) {
//                    locationText.setText(getCityFromLocation(getLatitude, getLongitude) + ", " + result.state);
//                } else {
//                    locationText.setText("Locating...");
//                }

                if (result.averageSpeedKph > 1) {
                    trafficSpeedVal = result.averageSpeedKph * .621371;
                    trafficSpeedVal = Math.round(trafficSpeedVal);
                    trafficSpeed.setText("" + String.format("%.0f", trafficSpeedVal) + "");
                } else {
                    trafficSpeed.setText("N/A");
                }

            } else {
                speedLimit.setText("N/A");
                speedLimitVal = 0;
            }
        }
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
//                        Toast.makeText(getApplicationContext(), "Bluetooth is Disabled", Toast.LENGTH_LONG).show();
                        break;

                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Toast.makeText(getApplicationContext(), "Bluetooth is Disabled", Toast.LENGTH_LONG).show();
                        break;

                    case BluetoothAdapter.STATE_ON:
//                        Toast.makeText(getApplicationContext(), "Bluetooth is Enabled", Toast.LENGTH_LONG).show();
                        break;

                    case BluetoothAdapter.STATE_TURNING_ON:
                        Toast.makeText(getApplicationContext(), "Bluetooth is Enabled", Toast.LENGTH_LONG).show();
                        //Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                        //startActivityForResult(getVisible, 0);
                        //pairedDevices = bluetoothAdapter.getBondedDevices();
//                        Intent intentOpenBluetoothSettings = new Intent();
//                        intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
//                        startActivity(intentOpenBluetoothSettings);
                        break;
                }
            }
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                //mArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                Toast.makeText(getApplicationContext(), device.getName() + "\n" + device.getAddress(), Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {

        currentTime = System.currentTimeMillis();

        try {
            getLatitude = location.getLatitude();
            getLongitude = location.getLongitude();
        } catch (Exception e) {}

        getSpeed(location);

        if (MainActivity.getInstance().gpsSwitch) {
            getSpeedLimit(location);
        }
        if (((elapsedTime % 10 == 0) && (currentSpeedMph > 0.1) || elapsedTime < 8) && (MainActivity.getInstance().gpsSwitch)) {
            getAddress();
        }

        if ((MainActivity.getInstance().newText) && (currentSpeedMph == 0)) {
            MainActivity.getInstance().newText = false;
            Intent intent = new Intent(this, SMSActivity.class);
            startActivity(intent);
        }

        getDistance(location);
        getAcceleration(location);
        getAltitude(location);
        getTime(location);
//        getTemp();
//        getHumidity();
//        setUpMap();
        if (MainActivity.getInstance().mapsSwitch) {
            updateMap(location);
        }

    }

    public void updateMap(Location location) {
        if ((((elapsedTime % 10) == 0) && (MainActivity.getInstance().mapsSwitch) && (currentSpeedMph > 2)) || ((!mapsEnabled) && (MainActivity.getInstance().mapsSwitch)) || ((elapsedTime < 10))) {

            mMapView.onResume();// needed to get the map to display immediately
            googleMap = mMapView.getMap();

            mapsEnabled = true;
            // latitude and longitude
            googleLatitude = getLatitude;
            googleLongitude = getLongitude;

            try {
                MapsInitializer.initialize(getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            // create marker
            marker = new MarkerOptions().position(
                    new LatLng(googleLatitude, googleLongitude)).title("Navigating");

            // Changing marker icon
            marker.icon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));

            // adding marker
            googleMap.addMarker(marker);
            if (!MainActivity.getInstance().speedCheckSwitch) {
                cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(googleLatitude, googleLongitude)).zoom(14).build();
            } else {
                cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(googleLatitude, googleLongitude)).zoom(13).build();
            }

            // Perform any camera updates here
            googleMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

            // GEOCODE STUFF
//            Polyline line = googleMap.addPolyline(new PolylineOptions()
//                    .add(new LatLng(getLatitude, getLongitude), new LatLng(MainActivity.getInstance().geocodeLatitude, MainActivity.getInstance().geocodeLongitude))
//                    .width(5)
//                    .color(getResources().getColor(R.color.red)));
            //googleMap.addPolyline(line);

        }
    }

    public void getAddress() {
        try {
//            if (((elapsedTime % 10 == 0) && (currentSpeedMph > -1)) || (elapsedTime < 5));
            String address = getAddressFromLocation(getLatitude, getLongitude);
            addressText.setText(address);
        } catch (Exception e) {}
    }

    public void setUpMap() {
        if (((elapsedTime % 10) == 0) && (MainActivity.getInstance().mapsSwitch) || ((elapsedTime == 3) && (MainActivity.getInstance().mapsSwitch))) {
//            Coordinates coordinates = new Coordinates(getLatitude, getLongitude);
//            mapView = new TTMapView(getApplicationContext(), coordinates, 18);
//            mapView.setTraffic(true);
//            // Fetch the Container for the Map
//            FrameLayout parentView = (FrameLayout) findViewById(R.id.parentMapLayout);
//            // And add it to the frame.
//            parentView.addView((View)mapView);
//            <FrameLayout
//            android:id="@+id/parentMapLayout"
//            android:layout_width="match_parent"
//            android:layout_height="match_parent"
//            android:layout_weight="0.25" >
//            </FrameLayout>
        }
    }

    public void getSpeedLimit(Location locationInput) {

        if (locationInput != null) {

            if (((elapsedTime % 10) == 0) && (currentSpeedMph > 3) || (!limitEnabled)) {
                limitEnabled = true;
                MyReverseGeocodeListener listener = new MyReverseGeocodeListener();
//            getLatitude = 42.259927;
//            getLongitude = -89.029033;
                Coordinates coordinates = new Coordinates(getLatitude, getLongitude);
                ReverseGeocodeOptionalParameters params = new ReverseGeocodeOptionalParameters();
                params.type = ReverseGeocodeOptionalParameters.REVERSE_TYPE_REGIONAL;
                ReverseGeocoder.reverseGeocode(coordinates, params, listener, null);
            }
        }
    }

    public void getSpeed(Location locationInput) {

        if (locationInput == null) {
            speedText.setText("-.- mph");
        } else {
            currentSpeedMpS = locationInput.getSpeed();
            currentSpeedMph = (currentSpeedMpS / 1609.34) * 3600;
            speedText.setText(String.format("%.2f", currentSpeedMph) + " mph");

            if (Math.floor(currentSpeedMph) == 0) {
                reStartTime = System.currentTimeMillis();

                splitTime = System.currentTimeMillis();
                previousSplit = System.currentTimeMillis();

                accTime = System.currentTimeMillis();
                previousAccTime = System.currentTimeMillis();

            } else {
                travelTime += currentTime - reStartTime;
            }

            if ((speedLimitVal > 4) && (currentSpeedMph > (speedLimitVal + 15))) {
                speedText.setTextColor(getResources().getColor(R.color.red));

                if (!sentText && MainActivity.getInstance().smsSwitch) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(MainActivity.getInstance().phoneNumber, null, "VES SpeedALERT:\nYou are currently driving 15mph over the speed limit. Use EXTREME caution.", null, null);
                    sentText = true;
                }

            } else if ((speedLimitVal > 4) && (currentSpeedMph > (speedLimitVal + 5))) {
                speedText.setTextColor(getResources().getColor(R.color.yellow));
                if (sentText) {
                    sentText = false;
                }
            } else if (speedLimitVal < 1) {
                speedText.setTextColor(getResources().getColor(R.color.white));
            } else {
                speedText.setTextColor(getResources().getColor(R.color.green));
                sentText = false;
            }
        }
    }

    public void getAcceleration(Location locationInput) {
        if (locationInput == null) {
            accelerationText.setText("-.- mph/s");
            accTime = System.currentTimeMillis();
            previousAccTime = System.currentTimeMillis();

        } else {
            accTime = System.currentTimeMillis();
            double speed = locationInput.getSpeed();
            double changeInSpeed = (speed - previousSpeed);

            double acceleration = 0;
            if (changeInSpeed == 0) {
                acceleration = 0;
            } else {
                acceleration = changeInSpeed / ((accTime - previousAccTime) / 1000);
                acceleration = (acceleration / 1609.34) * 3600;
            }
            if (acceleration < -30 || acceleration > 30) {
                acceleration = 0;
            }
            accelerationText.setText(String.format("%.2f", acceleration) + " mph/s");

            if (acceleration > 8 || acceleration < -8) {
                accelerationText.setTextColor(getResources().getColor(R.color.red));
            } else if (acceleration > 4 || acceleration < -4) {
                accelerationText.setTextColor(getResources().getColor(R.color.yellow));
            } else {
                accelerationText.setTextColor(getResources().getColor(R.color.green));
            }

            previousSpeed = speed;
            previousAccTime = accTime;
        }

    }

    public void getDistance(Location locationInput) {
        if (locationInput == null) {
            distanceText.setText("-.- mi");
            splitTime = System.currentTimeMillis();
            previousSplit = System.currentTimeMillis();
        } else {
            splitTime = System.currentTimeMillis();
            double instantaneousTime = (splitTime - previousSplit) / 1000;
            distance += ((locationInput.getSpeed() / 1609.34) * 3600) * (instantaneousTime / 3600);

            distanceText.setText(String.format("%.1f", distance) + " mi");
            previousSplit = splitTime;
        }
    }

    public void getAltitude(Location locationInput) {

        if (locationInput == null) {
            altitudeText.setText("-.- ft");
        } else {
            double height = locationInput.getAltitude();
            height = height * 3.28084;
            altitudeText.setText(String.format("%.1f", height) + " ft");
        }
    }

    public void getTime(Location locationInput) {
        if (locationInput == null) {
            timeText.setText("-.- sec");
        } else {
            currentTime = System.currentTimeMillis();
            elapsedTime = (currentTime - startTime) / 1000;
            //timeText.setText(String.format("%.0f", elapsedTime) + " sec");
            double hours = returnHours(elapsedTime); // elapsedTime? or travelTime?
            double minutes = returnMinutes(elapsedTime);
            double seconds = returnSeconds(elapsedTime);
            timeText.setText("H: " + String.format("%.0f", hours) + "  M: " + String.format("%.0f", minutes) + "  S: " + String.format("%.0f", seconds));
        }
    }

    public double returnHours(double elapsedTime) {
        double hours = Math.floor(elapsedTime / 3600);
        return hours;
    }

    public double returnMinutes(double elapsedTime) {
        double hours = Math.floor(elapsedTime / 3600);
        double minutes = Math.floor((elapsedTime - (hours * 3600)) / 60);
        return minutes;
    }

    public double returnSeconds(double elapsedTime) {
        double hours = Math.floor(elapsedTime / 3600);
        double minutes = Math.floor((elapsedTime - (hours * 3600)) / 60);
        double seconds = elapsedTime - (hours * 3600) - (minutes * 60);
        return seconds;
    }

    public void getTemp() {
        try {
            //String temperature = bluetoothReceivedMessage;
            //byte[] temperatureBytes = temperature.getBytes("US-ASCII");
            double tempVal = bluetoothData[10];
            tempVal = (tempVal * 1.8) + 32;
            tempText.setText(String.format("%.1f", tempVal) + "°F");
        } catch (Exception e) {
//            String temperature = "-.-";
//            tempText.setText(temperature);
        }
    }

    public void getHumidity() {
        try {
            //String humidity = bluetoothReceivedMessage;
            //byte[] humidityBytes = humidity.getBytes("US-ASCII");
            int humidityVal = bluetoothData[12];
            humidityText.setText("" + humidityVal + "%");

        } catch (Exception e) {
//            String humidity = "-.-";
//            humidityText.setText(humidity);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public String getAddressFromLocation(double geoLatitudeInput, double geoLongitudeInput) {

        String addressString = null;
        try {
            List<Address> address = geocoder.getFromLocation(geoLatitudeInput, geoLongitudeInput, 1);
//            String result = String.format("%s, %s, %s", address.get(0).getMaxAddressLineIndex() > 0 ? address.get(0).getAddressLine(0) : "", address.get(0).getAdminArea(), address.get(0).getCountryName());
//            String addressLine = address.get(0).getAddressLine(0);
//            String adminArea = address.get(0).getAdminArea();
//            String countryCode = address.get(0).getCountryCode();
//            String countryName = address.get(0).getCountryName();
//            String featureName = address.get(0).getFeatureName();
//            String locality = address.get(0).getLocality();
//            String phone = address.get(0).getPhone();
//            String postalCode = address.get(0).getPostalCode();
//            String premises = address.get(0).getPremises();
//            String subAdminArea = address.get(0).getSubAdminArea();
//            String subLocality = address.get(0).getSubLocality();
//            String subThoroughfare = address.get(0).getSubThoroughfare();
//            String thoroughfare = address.get(0).getThoroughfare();
//            String url = address.get(0).getUrl();
            addressString = address.get(0).getAddressLine(0) +"\n" + address.get(0).getLocality() + ", " + address.get(0).getAdminArea() + " " + address.get(0).getPostalCode();
        } catch (Exception e) {}
        return addressString;
    }

    public String getCityFromLocation(double geoLatitudeInput, double geoLongitudeInput) {

        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        String locality = null;
        try {
            List<Address> address = geocoder.getFromLocation(geoLatitudeInput, geoLongitudeInput, 1);
            String result = String.format("%s, %s, %s", address.get(0).getMaxAddressLineIndex() > 0 ? address.get(0).getAddressLine(0) : "", address.get(0).getAdminArea(), address.get(0).getCountryName());
            String addressLine = address.get(0).getAddressLine(0);
            String adminArea = address.get(0).getAdminArea();
            String countryCode = address.get(0).getCountryCode();
            String countryName = address.get(0).getCountryName();
            String featureName = address.get(0).getFeatureName();
            locality = address.get(0).getLocality();
            String phone = address.get(0).getPhone();
            String postalCode = address.get(0).getPostalCode();
            String premises = address.get(0).getPremises();
            String subAdminArea = address.get(0).getSubAdminArea();
            String subLocality = address.get(0).getSubLocality();
            String subThoroughfare = address.get(0).getSubThoroughfare();
            String thoroughfare = address.get(0).getThoroughfare();
            String url = address.get(0).getUrl();
        } catch (Exception e) {}
        return locality;
    }

    private class beepThread_distance extends Thread {

        volatile boolean running;

        public beepThread_distance() {
            running=true;
        }

        public void run() {
            while (running) {
                int min = -1;
                if (average_distance_1 == -1 && average_distance_2 == -1 && average_distance_3 == -1 && average_distance_4 == -1) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {

                    }
                } else {
                    int dis_1;
                    int dis_2;
                    int dis_3;
                    int dis_4;
                    if (average_distance_1 == -1) {
                        dis_1 = Integer.MAX_VALUE;
                    } else {
                        dis_1 = average_distance_1;
                    }
                    if (average_distance_2 == -1) {
                        dis_2 = Integer.MAX_VALUE;
                    } else {
                        dis_2 = average_distance_2;
                    }
                    if (average_distance_3 == -1) {
                        dis_3 = Integer.MAX_VALUE;
                    } else {
                        dis_3 = average_distance_3;
                    }
                    if (average_distance_4 == -1) {
                        dis_4 = Integer.MAX_VALUE;
                    } else {
                        dis_4 = average_distance_4;
                    }
                    int minimum = Math.min(dis_1, Math.min(dis_2, dis_4));
                    if(minimum < 125) {
                        toneG_distance.startTone(ToneGenerator.TONE_CDMA_HIGH_L, 100);
                    }
                    if (minimum > 100 && minimum < 125) {
                        try {
                            Thread.sleep(900);
                        } catch (InterruptedException e) {

                        }
                    } else if (minimum > 50 && minimum < 125) {
                        try {
                            Thread.sleep(450);
                        } catch (InterruptedException e) {

                        }
                    } else if (minimum > 10 && minimum < 125) {
                        try {
                            Thread.sleep(225);
                        } catch (InterruptedException e) {

                        }
                    } else {
                    }


                }
            }
        }

        public void cancel() {
            running=false;
        }
    }

    private class beepThread_speed extends Thread {

        volatile boolean running;

        public beepThread_speed() {
            running = true;
        }

        public void run() {
            while(running) {
                Log.v("LocationActivitySpeed", "Speed Limit is: " + speedLimitVal);
                Log.v("LocationActivitySpeed", "Current Speed is: " + currentSpeedMph);
                if ((speedLimitVal > 4) && (currentSpeedMph > (speedLimitVal + 15))) {
                    toneG_distance.startTone(ToneGenerator.TONE_CDMA_HIGH_L, 100);
                    try {
                        Thread.sleep(900);
                    } catch (InterruptedException e) {

                    }
                }
            }
        }
        public void cancel() {
            running = false;
        }
    }


    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

        public ConnectThread(BluetoothDevice device) {
            BluetoothSocket tmp = null;
            mmDevice = device;
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
            }
            mmSocket = tmp;
        }

        public void run() {
            Looper.prepare();
            bluetoothAdapter.cancelDiscovery();

            if(mConnectedThread!=null) {
                mConnectedThread.cancel();
                mConnectedThread=null;
            }
            try {
                mmSocket.connect();
            } catch (IOException connectException) {
                Log.e("LocationActivity", "Failed to connect to bluetooth");
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                }
                Log.e("LocationActivity","Reconnecting to socket...");
                setupBluetooth();
                return;
            }
            mConnectedThread = new ConnectedThread(mmSocket);
            mConnectedThread.start();
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        volatile boolean running;

        public ConnectedThread(BluetoothSocket socket) {
            running=true;
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            while(running) {
                byte[] buffer = new byte[14];
                byte[] data = new byte[14];
                verticalProgressBar1.setSecondaryProgress(0);
                verticalProgressBar2.setSecondaryProgress(0);
                //verticalProgressBar3.setSecondaryProgress(0);
                verticalProgressBar4.setSecondaryProgress(0);
                //int begin = 0;
                int bytes = 0;
                int[] distance_1_running = new int[6];
                int[] distance_2_running = new int[6];
                int[] distance_3_running = new int[6];
                int[] distance_4_running = new int[6];
                int count = 0;
                int num_fails = 0;
                boolean take_average = false;
                while (true) {
                    try {
                        if (mmInStream.available() >= 14) {
                            num_fails = 0;
                            bytes += mmInStream.read(buffer);
                            //if (!bluetoothSync) {
                            for (int i = 0; i < bytes; i++) {
                                if (buffer[i] == "}".getBytes()[0]) {
                                    begin = i;
                                    break;
                                }
                            }
                            //bluetoothSync = true;
                            //}
                            if (begin > 0) {
                                begin += 1;
                            }
                            for (int j = 0; j < 14; j++) {
                                data[j] = buffer[(j + begin) % 14];
                            }
                            bluetoothData = data;


                            int distance_1 = ((bluetoothData[1]) << 8) | (bluetoothData[2] & 0xff);
                            int distance_2 = ((bluetoothData[3]) << 8) | (bluetoothData[4] & 0xff);
                            int distance_3 = ((bluetoothData[5]) << 8) | (bluetoothData[6] & 0xff);
                            int distance_4 = ((bluetoothData[7]) << 8) | (bluetoothData[8] & 0xff);


                            int sum = 0;

                            distance_1_running[count] = distance_1;
                            distance_2_running[count] = distance_2;
                            distance_3_running[count] = distance_3;
                            distance_4_running[count] = distance_4;

                            if (take_average) {
                                for (int i = 0; i < 6; i++) {
                                    sum += distance_1_running[i];
                                }
                                average_distance_1 = sum / 6;

                                sum = 0;

                                for (int i = 0; i < 6; i++) {
                                    sum += distance_2_running[i];
                                }
                                average_distance_2 = sum / 6;

                                sum = 0;

                                for (int i = 0; i < 6; i++) {
                                    sum += distance_3_running[i];
                                }
                                average_distance_3 = sum / 6;

                                sum = 0;

                                for (int i = 0; i < 6; i++) {
                                    sum += distance_4_running[i];
                                }
                                average_distance_4 = sum / 6;

                                sum = 0;
                            } else {
                                average_distance_1 = distance_1;
                                average_distance_2 = distance_2;
                                average_distance_3 = distance_3;
                                average_distance_4 = distance_4;
                            }
                            int temp = bluetoothData[10];
                            int hum = bluetoothData[12];
                            Log.v("LocationActivity", "Distance 1 is: " + distance_1);
                            Log.v("LocationActivity", "Distance 2 is: " + distance_2);
                            Log.v("LocationActivity", "Distance 3 is: " + distance_3);
                            Log.v("LocationActivity", "Distance 4 is: " + distance_4);
                            Log.v("LocationActivity", "Temp is: " + temp);
                            Log.v("LocationActivity", "Humidity is: " + hum);
                            if (average_distance_1 == -1) {
                                verticalProgressBar1.setSecondaryProgress(100);
                                verticalProgressBar1.setProgress(0);
                            } else {
                                verticalProgressBar1.setProgress(Math.round(100 - (int) ((double) average_distance_1 / 1.5)));
                                verticalProgressBar1.setSecondaryProgress(0);
                            }

                            if (average_distance_2 == -1) {
                                verticalProgressBar2.setSecondaryProgress(100);
                                verticalProgressBar2.setProgress(0);
                            } else {
                                verticalProgressBar2.setProgress(Math.round(100 - (int) ((double) average_distance_2 / 1.5)));
                                verticalProgressBar2.setSecondaryProgress(0);
                            }

                            if (average_distance_3 == -1) {
                                //verticalProgressBar3.setSecondaryProgress(100);
                                //verticalProgressBar3.setProgress(0);
                            } else {
                                //verticalProgressBar3.setProgress(Math.round(100 - (int) ((double) average_distance_3 / 1.5)));
                                //verticalProgressBar3.setSecondaryProgress(0);
                            }

                            if (average_distance_4 == -1) {
                                verticalProgressBar4.setSecondaryProgress(100);
                                verticalProgressBar4.setProgress(0);
                            } else {
                                verticalProgressBar4.setProgress(Math.round(100 - (int) ((double) average_distance_4 / 1.5)));
                                verticalProgressBar4.setSecondaryProgress(0);
                            }

                            count = (count + 1) % 6;
                            if (count == 5) {
                                take_average = true;
                            }
                            getTemp();
                            getHumidity();

                        } else {
                            num_fails += 1;
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                            }
                            if (num_fails == 100) {
                                verticalProgressBar1.setProgress(0);
                                verticalProgressBar1.setSecondaryProgress(0);
                                verticalProgressBar2.setProgress(0);
                                verticalProgressBar2.setSecondaryProgress(0);
                                //verticalProgressBar3.setProgress(0);
                                //verticalProgressBar3.setSecondaryProgress(0);
                                verticalProgressBar4.setProgress(0);
                                verticalProgressBar4.setSecondaryProgress(0);
                                average_distance_1 = -1;
                                average_distance_2 = -1;
                                average_distance_3 = -1;
                                average_distance_4 = -1;
                                Log.e("LocationActivity", "Reconnecting");
                                mConnectThread = new ConnectThread(mDevice);
                                mConnectThread.start();
                            }
                        }

                    } catch (IOException e) {
                        break;
                    }
                }
            }
        }

        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
            }
        }

        public void cancel() {
            running=false;
            try {
                mmSocket.close();
            } catch (IOException e) {
            }
        }
    }

}