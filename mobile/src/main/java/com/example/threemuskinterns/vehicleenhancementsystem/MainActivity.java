package com.example.threemuskinterns.vehicleenhancementsystem;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Contacts;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Toast;

import com.example.threemuskinterns.vehicleenhancementsystem.fragments.HomeFragment;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    public CharSequence mTitle;
    public TelephonyManager telephoneManager;
    public String phoneNumber;
    Context context = this;
    public static MainActivity ins;
    public boolean mapsSwitch = false;
    public boolean trafficSwitch = false;
    public boolean speedCheckSwitch = false;
    public boolean smsSwitch = false;
    public boolean vesFullSwitch = false;
    public int switchesActivated = 0;
    public int numOfTexts = 0;
    public boolean gpsSwitch = false;
    public double geocodeLatitude;
    public double geocodeLongitude;
    public volatile String[] textList = new String[1000];
    public volatile String[] textContentsList = new String[1000];
    public volatile String[] numList = new String[1000];
    public boolean newText = false;
    public boolean locationActivityActive = false;

    public static MainActivity getInstance()
    {
        return ins;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ins = this;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = LayoutInflater.from(this);
        View view = inflator.inflate(R.layout.titleview_tele2, null);
        ((Tele2Text)view.findViewById(R.id.title)).setText("Home");
        this.getSupportActionBar().setCustomView(view);

//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
//                        .setDefaultFontPath("fonts/TELE2.TTF")
//                        .setFontAttrId(R.attr.fontPath)
//                        .build()
//        );
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        telephoneManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        phoneNumber = telephoneManager.getLine1Number();
    }

//    @Override
//    protected void attachBaseContext(Context newBase) {
//        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
//    }

    public void onStart() {
        super.onStart();
        //restoreVariables();
    }

    @Override
    public void onPause() {
        super.onPause();
        // saveVariables();
    }
    @Override
    public void onResume() {
        super.onResume();
        //restoreVariables();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        //saveVariables();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        fragmentManager.beginTransaction()
//                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
//                .commit();
        switch(position) {

            case 0:
                mTitle = "";
                setActionBarTitle("Home");
                setTitle("");
                getFragmentManager().popBackStack();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .addToBackStack("tag")
                        .commit();
                break;

            case 1:
//                if ((!MainActivity.getInstance().mapsSwitch && !MainActivity.getInstance().trafficSwitch && !MainActivity.getInstance().speedCheckSwitch && !MainActivity.getInstance().gpsSwitch&& !MainActivity.getInstance().smsSwitch)) {
                if (switchesActivated == 0) {
                    Toast.makeText(getApplicationContext(), "Please make a Selection", Toast.LENGTH_LONG).show();
                } else {
                    mTitle = "";
                    setActionBarTitle("Navigation");
                    setTitle("");
//                getFragmentManager().popBackStack();
//                getFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.container, new HomeFragment())
//                        .addToBackStack("tag")
//                        .commit();
                    Toast.makeText(getApplicationContext(), "Initializing Navigation...", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, LocationActivity.class);
                    startActivity(intent);
                }
                break;


            case 2:
                mTitle = "";
                setActionBarTitle("Messaging");
                setTitle("");
//                Fragment newFragment = new ThreadFragment();
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                fragmentTransaction.add(R.id.container, newFragment);
//                fragmentTransaction.commit();
//                Intent intent = new Intent(this, ThreadsActivity.class);
//                startActivity(intent);
                Intent intent = new Intent(this, SMSActivity.class);
                startActivity(intent);
                break;

            case 3:
                mTitle = "Quit";
                finish();
                break;

        }
    }

    public void onSectionAttached(int number) {
        String[] stringArray = getResources().getStringArray(R.array.section_titles);
        if (number >= 1) {
            mTitle = stringArray[number - 1];
        }
    }

    public void setActionBarTitle(String title){
        //getSupportActionBar().setTitle(title);
        LayoutInflater inflator = LayoutInflater.from(this);
        View view = inflator.inflate(R.layout.titleview_tele2, null);
        ((Tele2Text)view.findViewById(R.id.title)).setText(title);
        this.getSupportActionBar().setCustomView(view);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(false);    // previously true
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
//            //getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    public String getContactName(String phoneNumber)
    {
        Uri mBaseUri;
        Uri uri;
        String[] projection;
        mBaseUri = Contacts.Phones.CONTENT_FILTER_URL;
        projection = new String[] { android.provider.Contacts.People.NAME };
        try {
            Class<?> c =Class.forName("android.provider.ContactsContract$PhoneLookup");
            mBaseUri = (Uri) c.getField("CONTENT_FILTER_URI").get(mBaseUri);
            projection = new String[] { "display_name" };
        }
        catch (Exception e) {
        }

        uri = Uri.withAppendedPath(mBaseUri, Uri.encode(phoneNumber));
        Cursor cursor = this.getContentResolver().query(uri, projection, null, null, null);
        String contactName = "";

        if (cursor.moveToFirst())
        {
            contactName = cursor.getString(0);
        }

        cursor.close();
        cursor = null;

        return contactName;
    }

}
