package com.example.threemuskinterns.vehicleenhancementsystem;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Andrew on 7/17/2015.
 */
public class Tele2Text extends TextView {

    public Tele2Text(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/TELE2.TTF"));
    }
}
