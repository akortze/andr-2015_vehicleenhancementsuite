package com.example.threemuskinterns.vehicleenhancementsystem;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.example.threemuskinterns.vehicleenhancementsystem.CustomListViewAdapter;


public class SMSActivity extends ActionBarActivity {

    ListView listView;
    List<RowItem> rowItems;
    Button clearTexts;
    Button backButton;
    NeoText noSMS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);

//        showTextDialog(1, MainActivity.getInstance().textContentsList[0]);

        noSMS = (NeoText) findViewById(R.id.noSMS);
        backButton = (Button) findViewById(R.id.backButton);
        clearTexts = (Button) findViewById(R.id.clearTexts);

        clearTexts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTexts();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.getInstance().mTitle = "";
                MainActivity.getInstance().setActionBarTitle("Home");
                MainActivity.getInstance().setTitle("");
                finish();
            }
        });

        if (MainActivity.getInstance().numOfTexts == 0) {
            noSMS.setText("No Messages");
        } else {
            noSMS.setText("");
        }

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while(true) {
                        sleep(1000);
                        try {
                            if (MainActivity.getInstance().locationActivityActive && (LocationActivity.getInstance().currentSpeedMph > 2) ) {
                                finish();
                            }
                        } catch (Exception e) {}
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();


        // ACTIONBAR TEXT FONTS
        this.getSupportActionBar().setDisplayShowTitleEnabled(false);
        this.getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = LayoutInflater.from(this);
        View view = inflator.inflate(R.layout.titleview2_velo, null);
        ((VeloText) view.findViewById(R.id.title3)).setText("SMS Handling");
        this.getSupportActionBar().setCustomView(view);

        ////////////////////////////////////////////////////////////////////////////////////
        rowItems = new ArrayList<RowItem>();
        for (int i = 0; i < MainActivity.getInstance().numOfTexts; i++) {
            RowItem item = new RowItem(0);
            rowItems.add(item);
        }

        listView = (ListView) findViewById(R.id.list);
        CustomListViewAdapter adapter = new CustomListViewAdapter(this,
                R.layout.list_items, rowItems);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showTextDialog(position, MainActivity.getInstance().textList[position], MainActivity.getInstance().textContentsList[position]);
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                return true;
            }
        });
        listView.setAdapter(adapter);
        ///////////////////////////////////////////////////////////////////////////////////
    }

    public void showTextDialog(final int position, String sender, String sms) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View dialogView = layoutInflater.inflate(R.layout.text_background, null);
        TextView textText = (TextView) dialogView.findViewById(R.id.textText);
        textText.setText(sender + ": " + sms);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(dialogView);

        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("Call",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + MainActivity.getInstance().numList[position]));
                                startActivity(callIntent);
                            }
                        }).setNegativeButton("Dismiss",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    public void deleteTexts() {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View dialogView = layoutInflater.inflate(R.layout.fragment_delete_texts, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(dialogView);

        alertDialogBuilder.setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                MainActivity.getInstance().numOfTexts = 0;
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        }).setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        MainActivity.getInstance().mTitle = "";
        MainActivity.getInstance().setActionBarTitle("Home");
        MainActivity.getInstance().setTitle("");
        finish();
    }

}

