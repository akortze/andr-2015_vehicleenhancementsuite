package com.tomtom.lbs.sdk.demo;

import java.util.Locale;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.tomtom.lbs.sdk.TTMapView;
import com.tomtom.lbs.sdk.TTRouteLayer;
import com.tomtom.lbs.sdk.route.Route;
import com.tomtom.lbs.sdk.route.RouteData;
import com.tomtom.lbs.sdk.route.RouteInstruction;
import com.tomtom.lbs.sdk.route.RouteListener;
import com.tomtom.lbs.sdk.route.RouteParameters;
import com.tomtom.lbs.sdk.util.Coordinates;
import com.tomtom.lbs.sdk.util.SDKContext;

public class RoutesActivity extends Activity implements RouteListener {

	private TTMapView mapView;
	private Coordinates amsterdamCoordinates = new Coordinates(52.37, 4.89);
	private Coordinates mapCenter = amsterdamCoordinates;
	private Coordinates brusselsCoordinates = new Coordinates(50.85, 4.35);
	private TTRouteLayer routeLayer = null;
	private int zoomLevel = 15;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		SDKContext.setDeveloperKey("PLACE_YOUR_KEY_HERE");
		mapView = new TTMapView(getApplicationContext(), mapCenter, zoomLevel);
		setupMapContainer();
		calculateRoad();
	}

	private void setupMapContainer() {
		FrameLayout mapContainer = (FrameLayout) findViewById(R.id.mapContainer);
		mapContainer.addView(mapView);
	}

	private void calculateRoad() {
		zoomLevel = mapView.getZoom();
		setupTTRouteLayer();
		addRouteLayer(amsterdamCoordinates, brusselsCoordinates);
	}

	private void setupTTRouteLayer() {
		if (routeLayer != null)
			mapView.removeRouteLayer(routeLayer);
		routeLayer = new TTRouteLayer(null);
		routeLayer.setLineWidth(12);
		routeLayer.setOutlineWidth(4);
		routeLayer.setLineColor(Color.RED);
		routeLayer.setOutlineColor(Color.DKGRAY);
		mapView.addRouteLayer(routeLayer);
	}

	private void addRouteLayer(Coordinates from, Coordinates to) {
		RouteParameters routeParameters = new RouteParameters();
		routeParameters.dataPathZoomLevel = zoomLevel;
		routeParameters.includeDataPath = true;
		final RouteListener routeListener = this;
		final Coordinates[] points = new Coordinates[] { from, to };
		final Object payload = null;
		final String routeType = "Quickest";

		Route.getRouteWithPointsArray(points, routeType, routeParameters,
				routeListener, payload);
	}

	@Override
	public void handleRoute(RouteData routeData, Object object) {
		if (routeLayer == null)
			return;
		final Boolean showInstructionLocations = true;
		routeLayer.setRoute(routeData, showInstructionLocations);

		TextView instructionsTextView = (TextView) findViewById(R.id.instructionsTextView);
		instructionsTextView.setText(generateInstructionsText(routeData));
		TextView roadInformationTextView = (TextView) findViewById(R.id.roadInformationTextView);
		roadInformationTextView.setText(generateRoadInformationText(routeData));

		// Force mapView to redraw.
		mapView.invalidate();
	}

	private String generateRoadInformationText(RouteData routeData) {
		return String.format(Locale.US,
				"Instructions for %d minutes and %d Kms",
				routeData.summary.totalTimeSeconds / 60,
				routeData.summary.totalDistanceMeters / 1000);
	}

	private String generateInstructionsText(RouteData routeData) {
		StringBuilder instructionsText = new StringBuilder();
		for (RouteInstruction instruction : routeData.instructions) {
			instructionsText.append(instruction.text);
			instructionsText.append(" ");
			instructionsText.append(instruction.roadName);
			instructionsText.append("\n");
		}
		return instructionsText.toString();
	}
}
