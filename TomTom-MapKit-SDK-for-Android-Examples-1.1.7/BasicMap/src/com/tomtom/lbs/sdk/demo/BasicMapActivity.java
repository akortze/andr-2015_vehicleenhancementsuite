package com.tomtom.lbs.sdk.demo;

import com.tomtom.lbs.sdk.TTMapView;
import com.tomtom.lbs.sdk.util.Coordinates;
import com.tomtom.lbs.sdk.util.SDKContext;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class BasicMapActivity extends Activity {
	
	/* view objects */
	private TTMapView mapView;	
		
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        SDKContext.setDeveloperKey("PLACE_YOUR_KEY_HERE");
        
        // Seattle area    
        Coordinates coords = new Coordinates(47.6062,-122.3321);
        mapView = new TTMapView( getApplicationContext() , coords , 10 );
		mapView.setTraffic(true);
        		
		// Set the current View
        setContentView(R.layout.main);
        
		// Fetch the Container for the Map
        FrameLayout parentView = (FrameLayout) findViewById(R.id.parentMapLayout);
        
        // And add it to the frame.
		parentView.addView((View)mapView);
    }
}